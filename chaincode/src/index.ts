/*
 * SPDX-License-Identifier: Apache-2.0
 */

import { AirportcallsignContract } from './airportcallsign-contract';
export { AirportcallsignContract } from './airportcallsign-contract';

export const contracts: any[] = [ AirportcallsignContract ];
